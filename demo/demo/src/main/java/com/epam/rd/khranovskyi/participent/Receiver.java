package com.epam.rd.khranovskyi.participent;

import com.epam.rd.khranovskyi.config.ActiveMQConfig;
import com.epam.rd.khranovskyi.entity.Message;
import com.epam.rd.khranovskyi.entity.MessageReply;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Component
public class Receiver {
    private static final Logger LOGGER = Logger.getLogger("Receiver");

    List<Message> list = new ArrayList<>();

    @Autowired
    private JmsTemplate jmsTemplate;

    @JmsListener(destination = ActiveMQConfig.QUEUE)
    public void receiverMessage(@Payload Message message) {
        list.add(message);
        LOGGER.info("Adding to the collection...");
        if (list.size() == Producer.ITERATIONS) calculateAndReply();
    }

    public void calculateAndReply() {
        list.forEach(el -> {
            if ((int) (1 + Math.random() * 5) == 1) {
                LOGGER.info("Error occurred. UUID->" + el.getUuid());
                jmsTemplate.convertAndSend(ActiveMQConfig.REPLY, new MessageReply(el.getUuid(), el.getFirstVar() + el.getSecondVar() * 2));
            } else {
                jmsTemplate.convertAndSend(ActiveMQConfig.REPLY, new MessageReply(el.getUuid(), el.getFirstVar() + el.getSecondVar()));
            }
        });
    }
}
