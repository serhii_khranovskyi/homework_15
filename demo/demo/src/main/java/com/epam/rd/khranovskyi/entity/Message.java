package com.epam.rd.khranovskyi.entity;

import lombok.*;

import java.util.UUID;

@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private final UUID uuid = UUID.randomUUID();

    private Integer firstVar;

    private Integer secondVar;
}
