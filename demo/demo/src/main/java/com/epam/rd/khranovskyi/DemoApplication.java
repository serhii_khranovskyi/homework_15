package com.epam.rd.khranovskyi;

import com.epam.rd.khranovskyi.participent.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication implements ApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Autowired
    private Producer producer;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        producer.sendList();
    }
}
