package com.epam.rd.khranovskyi.participent;

import com.epam.rd.khranovskyi.config.ActiveMQConfig;
import com.epam.rd.khranovskyi.entity.MessageReply;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class ErrorReceiver {
    private static final Logger LOGGER = Logger.getLogger("ErrorReceiver");

    @JmsListener(destination = ActiveMQConfig.ERROR)
    public void receiveError(@Payload MessageReply message) {
        LOGGER.warning("Error in calculation ->" + message.toString());
    }
}
