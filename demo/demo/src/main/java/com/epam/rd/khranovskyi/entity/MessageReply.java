package com.epam.rd.khranovskyi.entity;

import lombok.*;

import java.util.UUID;

@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MessageReply {
    private UUID uuid;

    private Integer result;
}
