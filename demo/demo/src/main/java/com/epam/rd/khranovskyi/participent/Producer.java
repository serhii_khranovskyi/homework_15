package com.epam.rd.khranovskyi.participent;

import com.epam.rd.khranovskyi.config.ActiveMQConfig;
import com.epam.rd.khranovskyi.entity.Message;
import com.epam.rd.khranovskyi.entity.MessageReply;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Service
public class Producer {
    private static final Logger LOGGER = Logger.getLogger("Producer");
    public static final Integer ITERATIONS = 15;
    public List<Message> list = new ArrayList<>();

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendQueue(Message message) {
        jmsTemplate.convertAndSend(ActiveMQConfig.QUEUE, message);
        LOGGER.info("Send " + message.toString());
    }

    @JmsListener(destination = ActiveMQConfig.REPLY)
    public void receiveReply(@Payload MessageReply message) {
        if (checkResult(message)) {
            LOGGER.info("Ok. " + message.toString());
        } else {
            jmsTemplate.convertAndSend(ActiveMQConfig.ERROR, message);
        }
    }

    public boolean checkResult(MessageReply messageReply) {
        for (Message message : list) {
            if (message.getUuid().compareTo(messageReply.getUuid()) == 0) {
                if (messageReply.getResult() == (message.getFirstVar() + message.getSecondVar())) return true;
            }
        }
        return false;
    }

    public void sendList() {
        for (int i = 0; i < ITERATIONS; i++) {
            list.add(new Message(getRandomVar(), getRandomVar()));
            sendQueue(list.get(i));
        }
    }

    private Integer getRandomVar() {
        return (int) (Math.random() * 100);
    }
}
